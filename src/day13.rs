use std::{
    fmt::{Display, Formatter},
    str::FromStr,
};

#[derive(Clone)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl Direction {
    pub fn turn(&self, turn: &Turn) -> Direction {
        match turn {
            &Turn::Left => match &self {
                &Direction::Left => Direction::Down,
                &Direction::Right => Direction::Up,
                &Direction::Up => Direction::Left,
                &Direction::Down => Direction::Right,
            },
            &Turn::Right => match &self {
                &Direction::Left => Direction::Up,
                &Direction::Right => Direction::Down,
                &Direction::Up => Direction::Right,
                &Direction::Down => Direction::Left,
            },
            &Turn::Straight => self.clone(),
        }
    }

    pub fn right(&self) -> Direction {
        // /
        match &self {
            &Direction::Left => Direction::Down,
            &Direction::Right => Direction::Up,
            &Direction::Up => Direction::Right,
            &Direction::Down => Direction::Left,
        }
    }

    pub fn left(&self) -> Direction {
        // \
        match &self {
            &Direction::Left => Direction::Up,
            &Direction::Right => Direction::Down,
            &Direction::Up => Direction::Left,
            &Direction::Down => Direction::Right,
        }
    }
}

#[derive(Clone)]
enum Turn {
    Left,
    Right,
    Straight,
}

impl Turn {
    pub fn next(&self) -> Turn {
        match &self {
            &Turn::Left => Turn::Straight,
            &Turn::Straight => Turn::Right,
            &Turn::Right => Turn::Left,
        }
    }
}

#[derive(Clone)]
struct Cart {
    direction: Direction,
    turn: Turn,
}

impl Display for Cart {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        match self.direction {
            Direction::Left => write!(f, "<"),
            Direction::Right => write!(f, ">"),
            Direction::Up => write!(f, "^"),
            Direction::Down => write!(f, "v"),
        }
    }
}

// Turns are regard coming from up or down.
#[derive(Clone)]
enum CellType {
    Empty,
    StraightUpDown,
    StraightLeftRight,
    TurnRight,
    TurnLeft,
    Intersection,
}

impl Display for CellType {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        match self {
            CellType::Empty => write!(f, " "),
            CellType::StraightUpDown => write!(f, "|"),
            CellType::StraightLeftRight => write!(f, "-"),
            CellType::TurnRight => write!(f, "/"),
            CellType::TurnLeft => write!(f, "\\"),
            CellType::Intersection => write!(f, "+"),
        }
    }
}

#[derive(Clone)]
struct Cell {
    cell_type: CellType,
    cart: Option<Cart>,
}

impl Display for Cell {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        match &self.cart {
            &Some(ref c) => write!(f, "{}", c),
            &None => write!(f, "{}", self.cell_type),
        }
    }
}

impl From<char> for Cell {
    fn from(c: char) -> Self {
        match c {
            '|' => Cell {
                cell_type: CellType::StraightUpDown,
                cart: None,
            },
            '-' => Cell {
                cell_type: CellType::StraightLeftRight,
                cart: None,
            },
            '+' => Cell {
                cell_type: CellType::Intersection,
                cart: None,
            },
            '/' => Cell {
                cell_type: CellType::TurnRight,
                cart: None,
            },
            '\\' => Cell {
                cell_type: CellType::TurnLeft,
                cart: None,
            },
            '>' => Cell {
                cell_type: CellType::StraightLeftRight,
                cart: Some(Cart {
                    direction: Direction::Right,
                    turn: Turn::Left,
                }),
            },
            '<' => Cell {
                cell_type: CellType::StraightLeftRight,
                cart: Some(Cart {
                    direction: Direction::Left,
                    turn: Turn::Left,
                }),
            },
            '^' => Cell {
                cell_type: CellType::StraightUpDown,
                cart: Some(Cart {
                    direction: Direction::Up,
                    turn: Turn::Left,
                }),
            },
            'v' => Cell {
                cell_type: CellType::StraightUpDown,
                cart: Some(Cart {
                    direction: Direction::Down,
                    turn: Turn::Left,
                }),
            },
            _ => Cell {
                cell_type: CellType::Empty,
                cart: None,
            },
        }
    }
}

#[derive(Clone)]
struct Grid {
    grid: Vec<Vec<Cell>>,
    w: usize,
    h: usize,
}

impl Display for Grid {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        for row in &self.grid {
            for cell in row {
                write!(f, "{}", cell)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl FromStr for Grid {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let grid: Vec<Vec<Cell>> = s
            .lines()
            .map(|l| l.chars().map(|c| c.into()).collect())
            .collect();
        let h = grid.len();
        let w = grid[0].len();
        Ok(Grid { grid, w, h })
    }
}

impl Grid {
    pub fn step(&mut self, remove: bool) -> Option<(usize, usize)> {
        let mut next = self.grid.clone();
        for y in 0..self.h {
            for x in 0..self.w {
                if self.grid.get(y).and_then(|r| r.get(x)).is_none() {
                    continue;
                }
                if let Some(c) = self.grid[y][x].cart.clone() {
                    let (x2, y2) = match c.direction {
                        Direction::Left => (x - 1, y),
                        Direction::Right => (x + 1, y),
                        Direction::Up => (x, y - 1),
                        Direction::Down => (x, y + 1),
                    };
                    if let CellType::Empty = next[y2][x2].cell_type {
                        panic!("{} {} is empty", x2, y2);
                    }
                    let mut c = c.clone();
                    match self.grid[y2][x2].cell_type {
                        CellType::TurnRight => c.direction = c.direction.right(),
                        CellType::TurnLeft => c.direction = c.direction.left(),
                        CellType::Intersection => {
                            c.direction = c.direction.turn(&c.turn);
                            c.turn = c.turn.next();
                        }
                        _ => {}
                    }
                    if next[y2][x2].cart.is_some() {
                        if remove {
                            next[y][x].cart = None;
                            next[y2][x2].cart = None;
                            self.grid[y][x].cart = None;
                            self.grid[y2][x2].cart = None;
                        } else {
                            return Some((x2, y2));
                        }
                    } else {
                        next[y][x].cart = None;
                        next[y2][x2].cart = Some(c);
                    }
                }
            }
        }
        if remove {
            let mut count = 0;
            let mut pos = (0, 0);
            for y3 in 0..self.h {
                for x3 in 0..self.w {
                    if next
                        .get(y3)
                        .and_then(|r| r.get(x3))
                        .map(|c| c.cart.is_some())
                        .unwrap_or(false)
                    {
                        count += 1;
                        pos = (x3, y3);
                    }
                }
            }
            if count == 1 {
                return Some(pos);
            }
        }
        self.grid = next;
        None
    }
}

#[aoc_generator(day13)]
fn parse(input: &str) -> Box<Grid> {
    Box::new(input.parse().unwrap())
}

#[aoc(day13, part1)]
fn part1(input: &Grid) -> String {
    let mut grid = input.clone();
    let mut res = None;
    while res.is_none() {
        res = grid.step(false);
    }
    let (x, y) = res.unwrap();
    format!("{},{}", x, y)
}

#[aoc(day13, part2)]
fn part2(input: &Grid) -> String {
    let mut grid = input.clone();
    let mut res = None;
    while res.is_none() {
        res = grid.step(true);
    }
    let (x, y) = res.unwrap();
    format!("{},{}", x, y)
}
