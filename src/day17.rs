use colored::*;
use regex::Regex;
use std::{collections::HashSet, fmt, thread, time::Duration};

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
enum Direction {
    Left,
    Right,
    Down,
}

impl Direction {
    pub fn next(&self, x: isize, y: isize) -> (isize, isize) {
        match self {
            Direction::Left => (x - 1, y),
            Direction::Right => (x + 1, y),
            Direction::Down => (x, y + 1),
        }
    }
}

#[derive(Debug, Clone)]
struct Grid {
    clay: HashSet<(isize, isize)>,
    flowing: HashSet<(isize, isize, Direction)>,
    water: HashSet<(isize, isize)>,
    sink: (isize, isize),
    x: (isize, isize),
    y: (isize, isize),
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for y in self.y.0..=self.y.1 {
            for x in self.x.0..=self.x.1 {
                if self.clay.contains(&(x, y)) {
                    write!(f, "🧱 ")?;
                } else if self.flowing.contains(&(x, y)) {
                    write!(f, "💧")?;
                } else if self.water.contains(&(x, y)) {
                    write!(f, "{}", "██".blue())?;
                } else if self.sink == (x, y) {
                    write!(f, "🌊")?;
                } else {
                    write!(f, "{}", "██".bright_black())?;
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Grid {
    pub fn new(sinkx: isize, sinky: isize) -> Grid {
        Grid {
            clay: HashSet::new(),
            water: HashSet::new(),
            flowing: HashSet::new(),
            sink: (sinkx, sinky),
            x: (sinkx, sinkx),
            y: (sinky, sinky),
        }
    }

    pub fn add_clay(&mut self, x: isize, y: isize) {
        self.x.0 = self.x.0.min(x);
        self.x.1 = self.x.1.max(x);
        self.y.0 = self.y.0.min(y);
        self.y.1 = self.y.1.max(y);
        self.clay.insert((x, y));
    }

    pub fn step(&mut self, sink: bool) {
        let mut flowing = HashSet::new();
        let mut water = self.water.clone();
        for &(x, y, dir) in &self.flowing {
            let next = dir.next(x, y);
            if self.clay.contains(&next) || self.water.contains(&next) {
                water.insert((x, y));
            } else {
                flowing.insert((next.0, next.1, dir));
            }
        }
        if sink {
            flowing.insert((self.sink.0, self.sink.1 + 1, Direction::Down));
        }
        println!("{:?} {:?}", self.x, self.y);
        self.water = water;
        self.flowing = flowing
            .iter()
            .cloned()
            .filter(|&(x, y, dir)| !self.water.contains(&(x, y)))
            .filter(|&(x, y, dir)| {
                (x >= self.x.0) && (x <= self.x.1) && (y >= self.y.0) && (y <= self.y.1)
            })
            .collect();
    }
}

#[aoc_generator(day17)]
fn parse(input: &str) -> Box<Grid> {
    let re = Regex::new(r"(.)=(\d*).*=(\d*)\.\.(\d*)").unwrap();
    let mut grid = Grid::new(500, 0);
    for capt in re.captures_iter(input) {
        let a = capt[2].parse::<isize>().unwrap();
        let b = capt[3].parse::<isize>().unwrap();
        let c = capt[4].parse::<isize>().unwrap();
        match &capt[1] {
            "x" => {
                for y in b..=c {
                    grid.add_clay(a, y);
                }
            }
            "y" => {
                for x in b..=c {
                    grid.add_clay(x, a);
                }
            }
            _ => unimplemented!(),
        }
    }
    Box::new(grid)
}

#[aoc(day17, part1)]
fn part1(grid: &Grid) -> isize {
    let mut grid = grid.clone();
    println!("{}", grid);
    for _ in 0..100 {
        grid.step(true);
        println!("{}", grid);
    }
    for _ in 0..20 {
        grid.step(false);
        println!("{}", grid);
    }
    0
}
