use std::fmt;

struct RecipeList {
    pub list: Vec<u8>,
    positions: [usize; 2],
}

impl RecipeList {
    pub fn new() -> RecipeList {
        RecipeList {
            list: vec![3, 7],
            positions: [0, 1],
        }
    }

    pub fn step(&mut self) {
        let scores = self
            .positions
            .iter()
            .map(|&x| self.list[x])
            .collect::<Vec<_>>();
        let sum = scores.iter().sum::<u8>();
        if sum >= 10 {
            self.list.push(1);
        }
        self.list.push(sum % 10);
        let len = self.len();
        for i in 0..self.positions.len() {
            self.positions[i] = (self.positions[i] + 1 + scores[i] as usize) % len;
        }
    }

    pub fn len(&self) -> usize {
        self.list.len()
    }
}

impl fmt::Display for RecipeList {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0..self.list.len() {
            if i == self.positions[0] {
                write!(f, "({})", self.list[i])?;
            } else if i == self.positions[1] {
                write!(f, "[{}]", self.list[i])?;
            } else {
                write!(f, " {} ", self.list[i])?;
            }
        }
        Ok(())
    }
}

#[aoc(day14, part1)]
fn part1(input: &str) -> String {
    let num = input.trim().parse().unwrap();
    let mut recipes = RecipeList::new();
    while recipes.len() < num + 10 {
        recipes.step();
    }
    recipes.list[num..(num + 10)]
        .iter()
        .fold(String::new(), |acc, x| format!("{}{}", acc, x))
}

#[aoc(day14, part2)]
fn part2(input: &str) -> usize {
    let pattern = input
        .trim()
        .chars()
        .map(|x| x.to_digit(10).unwrap() as u8)
        .collect::<Vec<_>>();
    let mut recipes = RecipeList::new();
    loop {
        recipes.step();
        if recipes.len() >= pattern.len() {
            if recipes.list[(recipes.len() - pattern.len())..recipes.len()] == pattern[..] {
                return recipes.len() - pattern.len();
            }
            if recipes.len() >= pattern.len() + 1 {
                if recipes.list[(recipes.len() - pattern.len() - 1)..(recipes.len() - 1)]
                    == pattern[..]
                {
                    return recipes.len() - pattern.len() - 1;
                }
            }
        }
    }
}
