use rayon::prelude::*;
use regex::Regex;

#[derive(Debug)]
struct Rect {
    id: usize,
    l: usize,
    t: usize,
    r: usize,
    b: usize,
}

impl Rect {
    pub fn overlaps_with(&self, rect: &Rect) -> bool {
        if (self.l > rect.r) || (rect.l > self.r) {
            return false;
        }
        if (self.b < rect.t) || (rect.b < self.t) {
            return false;
        }
        true
    }
}

#[aoc_generator(day3)]
fn gen_rect(input: &str) -> Vec<Rect> {
    let re = Regex::new(r"#\s*(\d*)\s*@\s*(\d*),(\d*):\s*(\d*)x(\d*)").unwrap();
    re.captures_iter(input)
        .map(|c| {
            let id = c[1].parse().unwrap();
            let x = c[2].parse().unwrap();
            let y = c[3].parse().unwrap();
            let w: usize = c[4].parse().unwrap();
            let h: usize = c[5].parse().unwrap();
            Rect {
                id,
                l: x,
                t: y,
                r: x + w,
                b: y + h,
            }
        }).collect()
}

#[aoc(day3, part1)]
fn part1(input: &Vec<Rect>) -> usize {
    let mut w = 0;
    let mut h = 0;
    for r in input {
        w = w.max(r.r);
        h = h.max(r.b);
    }
    println!("{}x{}", w, h);
    let mut area: Vec<Vec<usize>> = vec![vec![0; w]; h];
    for r in input {
        for x in r.l..r.r {
            for y in r.t..r.b {
                area[y][x] += 1;
            }
        }
    }
    area.par_iter()
        .map(|x| {
            x.par_iter()
                .map(|&y| if y > 1 { 1 } else { 0 })
                .sum::<usize>()
        }).sum::<usize>()
}

#[aoc(day3, part2)]
fn part2(input: &Vec<Rect>) -> usize {
    let l = input.len();
    'outer: for a in 0..(l - 1) {
        for b in 0..l {
            if a == b {
                continue;
            }
            if input[a].overlaps_with(&input[b]) {
                continue 'outer;
            }
        }
        return input[a].id;
    }
    unreachable!()
}
