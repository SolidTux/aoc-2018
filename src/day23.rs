use regex::Regex;
use std::collections::{HashMap, HashSet};

#[derive(Debug)]
struct Bot {
    pub pos: [isize; 3],
    pub range: usize,
}

fn manhattan(a: &[isize], b: &[isize]) -> usize {
    a.iter()
        .zip(b.iter())
        .map(|(&a, &b)| (a - b).abs() as usize)
        .sum()
}

fn line_inter(a: isize, ra: usize, b: isize, rb: usize) -> Vec<isize> {
    if b > a {
        ((b - rb as isize)..(a + ra as isize)).collect()
    } else {
        ((a - ra as isize)..(b + rb as isize)).collect()
    }
}

impl Bot {
    fn distance(&self, other: &Bot) -> usize {
        manhattan(&self.pos, &other.pos)
    }

    fn intersection(&self, other: &Bot) -> HashSet<[isize; 3]> {
        let mut res = HashSet::new();
        for x in line_inter(self.pos[0], self.range, other.pos[0], other.range) {
            for y in line_inter(self.pos[1], self.range, other.pos[1], other.range) {
                for z in line_inter(self.pos[2], self.range, other.pos[2], other.range) {
                    if manhattan(&[x, y, z], &self.pos) <= self.range
                        && manhattan(&[x, y, z], &other.pos) <= other.range
                    {
                        res.insert([x, y, z]);
                    }
                }
            }
        }
        res
    }
}

#[aoc_generator(day23)]
fn parse(input: &str) -> Vec<Bot> {
    let re = Regex::new(r"pos=<(-?\d*),(-?\d*),(-?\d*)>,\s*r=(-?\d*)").unwrap();
    re.captures_iter(input)
        .map(|c| Bot {
            pos: [
                c[1].parse().unwrap(),
                c[2].parse().unwrap(),
                c[3].parse().unwrap(),
            ],
            range: c[4].parse().unwrap(),
        })
        .collect()
}

#[aoc(day23, part1)]
fn part1(input: &[Bot]) -> usize {
    let largest = input.iter().max_by_key(|&b| b.range).unwrap();
    let mut res = 0;
    for b in input {
        if largest.distance(b) <= largest.range {
            res += 1;
        }
    }
    res
}

#[aoc(day23, part2)]
fn part2(input: &[Bot]) -> usize {
    let mut res = HashMap::new();
    for i in 0..input.len() {
        for j in (i + 1)..input.len() {
            for pos in input[i].intersection(&input[j]) {
                let e = res.entry(pos.clone()).or_insert(HashSet::new());
                (*e).insert(i);
                (*e).insert(j);
            }
        }
    }
    for (k, v) in res.iter_mut() {
        for i in 0..input.len() {
            if manhattan(&input[i].pos, k) <= input[i].range {
                v.insert(i);
            }
        }
    }
    let num = res.values().map(|v| v.len()).max().unwrap();
    let pos = res
        .iter()
        .filter_map(|(k, v)| {
            if v.len() == num {
                Some(k.clone())
            } else {
                None
            }
        })
        .collect::<HashSet<_>>();
    println!("{:?}", pos);
    pos.iter().map(|x| manhattan(x, &[0, 0, 0])).max().unwrap()
}
