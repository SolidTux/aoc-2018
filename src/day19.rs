use regex::Regex;
use std::str::FromStr;

const REGS: usize = 6;

struct Cpu {
    pub registers: [usize; REGS],
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            registers: [0; REGS],
        }
    }

    pub fn execute(&mut self, instr: &Instruction, data: &[usize]) {
        match instr {
            Instruction::Addr => {
                self.registers[data[2]] = self.registers[data[0]] + self.registers[data[1]]
            }
            Instruction::Addi => self.registers[data[2]] = self.registers[data[0]] + data[1],
            Instruction::Mulr => {
                self.registers[data[2]] = self.registers[data[0]] * self.registers[data[1]]
            }
            Instruction::Muli => self.registers[data[2]] = self.registers[data[0]] * data[1],
            Instruction::Banr => {
                self.registers[data[2]] = self.registers[data[0]] & self.registers[data[1]]
            }
            Instruction::Bani => self.registers[data[2]] = self.registers[data[0]] & data[1],
            Instruction::Borr => {
                self.registers[data[2]] = self.registers[data[0]] | self.registers[data[1]]
            }
            Instruction::Bori => self.registers[data[2]] = self.registers[data[0]] | data[1],
            Instruction::Setr => self.registers[data[2]] = self.registers[data[0]],
            Instruction::Seti => self.registers[data[2]] = data[0],
            Instruction::Gtir => {
                if data[0] > self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Gtri => {
                if self.registers[data[0]] > data[1] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Gtrr => {
                if self.registers[data[0]] > self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Eqir => {
                if data[0] == self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Eqri => {
                if self.registers[data[0]] == data[1] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Eqrr => {
                if self.registers[data[0]] == self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
        }
    }
}

#[derive(Hash, PartialEq, Eq, Debug, Clone)]
enum Instruction {
    Addr,
    Addi,
    Mulr,
    Muli,
    Banr,
    Bani,
    Borr,
    Bori,
    Setr,
    Seti,
    Gtir,
    Gtri,
    Gtrr,
    Eqir,
    Eqri,
    Eqrr,
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "addr" => Ok(Instruction::Addr),
            "addi" => Ok(Instruction::Addi),
            "mulr" => Ok(Instruction::Mulr),
            "muli" => Ok(Instruction::Muli),
            "banr" => Ok(Instruction::Banr),
            "bani" => Ok(Instruction::Bani),
            "borr" => Ok(Instruction::Borr),
            "bori" => Ok(Instruction::Bori),
            "setr" => Ok(Instruction::Setr),
            "seti" => Ok(Instruction::Seti),
            "gtir" => Ok(Instruction::Gtir),
            "gtri" => Ok(Instruction::Gtri),
            "gtrr" => Ok(Instruction::Gtrr),
            "eqir" => Ok(Instruction::Eqir),
            "eqri" => Ok(Instruction::Eqri),
            "eqrr" => Ok(Instruction::Eqrr),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone)]
struct Program {
    instructions: Vec<(Instruction, [usize; 3])>,
    ip_reg: usize,
}

impl Program {
    pub fn run(&mut self, cpu: &mut Cpu, ip_start: usize) {
        let mut ip = ip_start;
        while ip < self.instructions.len() {
            cpu.registers[self.ip_reg] = ip;
            let (instr, data) = self.instructions[ip].clone();
            print!(
                "ip={} {:?} {:?} {} {} {} ",
                ip, cpu.registers, instr, data[0], data[1], data[2]
            );
            cpu.execute(&instr, &data);
            println!("{:?}", cpu.registers);
            ip = cpu.registers[self.ip_reg];
            ip += 1;
        }
    }
}

#[aoc_generator(day19)]
fn parse(input: &str) -> Box<Program> {
    let re_ip = Regex::new(r"#ip (\d*)").unwrap();
    let ip_reg = re_ip
        .captures(input)
        .unwrap()
        .get(1)
        .unwrap()
        .as_str()
        .parse()
        .unwrap();
    let re = Regex::new(r"(\w*) (\d*) (\d*) (\d*)").unwrap();
    let instructions = re
        .captures_iter(input)
        .map(|c| {
            (
                c[1].parse().unwrap(),
                [
                    c[2].parse().unwrap(),
                    c[3].parse().unwrap(),
                    c[4].parse().unwrap(),
                ],
            )
        })
        .collect();
    Box::new(Program {
        instructions,
        ip_reg,
    })
}

#[aoc(day19, part1)]
fn part1(prog: &Program) -> usize {
    let mut prog = prog.clone();
    let mut cpu = Cpu::new();
    prog.run(&mut cpu, 0);
    cpu.registers[0]
}

#[aoc(day19, part2)]
fn part2(prog: &Program) -> usize {
    // TODO automatic solution
    let mut prog = prog.clone();
    let mut cpu = Cpu::new();
    //cpu.registers = [1, 0, 0, 0, 0, 0];
    //cpu.registers = [0, 3, 10551408, 10551408, 1, 0];
    //cpu.registers = [1, 3, 5275704, 10551408, 2, 0];
    //cpu.registers = [3, 3, 5275704, 10551408, 2, 0];
    //cpu.registers = [5, 3, 10551408, 10551408, 2, 0];
    //cpu.registers = [5, 3, 3517136, 10551408, 3, 0];
    //cpu.registers = [8, 3, 10551408, 10551408, 3, 0];
    //cpu.registers = [8, 3, 2637852, 10551408, 4, 0];
    cpu.registers = [32188416, 3, 10551408, 10551408, 10551408, 0];
    prog.run(&mut cpu, 3);
    cpu.registers[0]
}
