use failure::Error;
use std::{
    collections::{HashMap, HashSet},
    fmt,
    str::FromStr,
};

#[derive(Clone, PartialEq)]
enum Cell {
    Empty,
    Unit(Unit),
    Wall,
}

impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Cell::Empty => write!(f, "  "),
            Cell::Unit(u) => write!(f, "{}", u),
            Cell::Wall => write!(f, "██"),
        }
    }
}

#[derive(Clone, PartialEq)]
struct Unit {
    species: Species,
    pub hp: usize,
}

impl fmt::Display for Unit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.species)
    }
}

#[derive(Clone, PartialEq, Hash, Eq)]
enum Species {
    Goblin,
    Elf,
}

impl fmt::Display for Species {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Species::Goblin => write!(f, "👺"),
            Species::Elf => write!(f, "👱"),
        }
    }
}

#[derive(Clone)]
struct Grid {
    cells: Vec<Cell>,
    w: usize,
    h: usize,
    pub attack: HashMap<Species, usize>,
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0..(self.w * self.h) {
            write!(f, "{}", self.cells[i as usize])?;
            if i % self.w == self.w - 1 {
                writeln!(f)?;
            }
        }
        writeln!(f)?;
        let mut c = 0;
        let mut counts = HashMap::new();
        for i in 0..(self.w * self.h) {
            if let Cell::Unit(u) = &self.cells[i as usize] {
                write!(
                    f,
                    "{} {:>3}🧡 {:>3}🗡    ",
                    u, u.hp, self.attack[&u.species]
                )?;
                if c % 3 == 2 {
                    writeln!(f)?;
                }
                c += 1;
                let e = counts.entry(u.species.clone()).or_insert(0);
                *e += 1;
            }
        }
        if c % 3 != 1 {
            println!();
        }
        println!();
        for (s, c) in counts {
            print!("{:>3}x{} ", s, c);
        }
        println!();
        Ok(())
    }
}

impl FromStr for Grid {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let w = s
            .lines()
            .next()
            .ok_or(format_err!("Could not split lines."))?
            .trim()
            .len();
        let h = s.lines().count();
        let cells = s
            .chars()
            .filter_map(|c| match c {
                '#' => Some(Cell::Wall),
                '.' => Some(Cell::Empty),
                'G' => Some(Cell::Unit(Unit {
                    species: Species::Goblin,
                    hp: 200,
                })),
                'E' => Some(Cell::Unit(Unit {
                    species: Species::Elf,
                    hp: 200,
                })),
                _ => None,
            })
            .collect();
        let mut attack = HashMap::new();
        attack.insert(Species::Goblin, 3);
        attack.insert(Species::Elf, 3);
        Ok(Grid {
            cells,
            w,
            h,
            attack,
        })
    }
}

impl Grid {
    fn enemies(&self, species: &Species) -> Vec<usize> {
        (0..(self.w * self.h))
            .filter(|&x| {
                if let Cell::Unit(ref u) = self.cells[x as usize] {
                    u.species != *species
                } else {
                    false
                }
            })
            .collect()
    }

    fn adjacent_empty_cells(&self, pos: usize) -> Vec<usize> {
        let i = pos as i32;
        [i - self.w as i32, i - 1, i + 1, i + self.w as i32]
            .iter()
            .cloned()
            .filter(|&x| x > 0)
            .filter(|&x| self.cells[x as usize] == Cell::Empty)
            .map(|x| x as usize)
            .collect()
    }

    fn adjacent(&self, a: usize, b: usize) -> bool {
        (b == a - 1) || (b == a + 1) || (b == a - self.w) || (b == a + self.w)
    }

    fn nearest(&self, targets: &[usize], start: usize) -> Option<(usize, usize)> {
        let mut visited = HashSet::new();
        let mut positions = HashSet::new();
        visited.insert(start);
        positions.insert(start);
        let mut distance = 0;
        let mut res = Vec::new();
        let mut finished = false;
        while !finished {
            distance += 1;
            let mut tmp = HashSet::new();
            for p in positions {
                let next = self
                    .adjacent_empty_cells(p)
                    .iter()
                    .filter(|x| !visited.contains(x))
                    .cloned()
                    .collect::<Vec<_>>();
                for n in next {
                    if targets.contains(&n) {
                        res.push(n);
                        finished = true;
                    } else {
                        tmp.insert(n);
                        visited.insert(n);
                    }
                }
            }
            positions = tmp;
            if positions.is_empty() {
                break;
            }
        }
        Some((*(res.iter().min()?), distance))
    }

    fn distance(&self, a: usize, b: usize) -> Option<usize> {
        if a == b {
            return Some(0);
        }
        let mut visited = HashSet::new();
        let mut positions = HashSet::new();
        visited.insert(a);
        positions.insert(a);
        let mut distance = 0;
        loop {
            distance += 1;
            let mut tmp = HashSet::new();
            for p in positions {
                let next = self
                    .adjacent_empty_cells(p)
                    .iter()
                    .filter(|x| !visited.contains(x))
                    .cloned()
                    .collect::<Vec<_>>();
                for n in next {
                    if n == b {
                        return Some(distance);
                    } else {
                        tmp.insert(n);
                        visited.insert(n);
                    }
                }
            }
            positions = tmp;
            if positions.is_empty() {
                return None;
            }
        }
    }

    fn next(&self, target: usize, start: usize, distance: usize) -> Option<usize> {
        self.adjacent_empty_cells(start)
            .iter()
            .filter(|&x| {
                if let Some(d) = self.distance(*x, target) {
                    d == distance - 1
                } else {
                    false
                }
            })
            .min()
            .cloned()
    }

    pub fn step(&mut self, part2: bool) -> Option<Species> {
        let mut moved = Vec::new();
        for i in 0..(self.w * self.h) {
            if moved.contains(&i) {
                continue;
            }
            let unit = if let Cell::Unit(ref u) = self.cells[i as usize] {
                u.clone()
            } else {
                continue;
            };

            // Get list of enemies
            let enemies = self.enemies(&unit.species);
            if enemies.is_empty() {
                return Some(unit.species);
            }
            let mut adj_en = enemies
                .iter()
                .filter(|&x| self.adjacent(i, *x))
                .cloned()
                .collect::<Vec<_>>();
            if adj_en.is_empty() {
                let targets = enemies
                    .iter()
                    .map(|&x| self.adjacent_empty_cells(x))
                    .flatten()
                    .collect::<Vec<_>>();

                // Get nearest target
                if let Some((target, dis)) = self.nearest(&targets, i) {
                    if let Some(next) = self.next(target, i, dis) {
                        self.cells[next] = Cell::Unit(unit.clone());
                        self.cells[i] = Cell::Empty;
                        moved.push(next);
                        adj_en = enemies
                            .iter()
                            .filter(|&x| self.adjacent(next, *x))
                            .cloned()
                            .collect::<Vec<_>>();
                    }
                }
            }
            if let Some((e, _)) = adj_en
                .iter()
                .filter_map(|&x| {
                    if let Cell::Unit(ref u) = self.cells[x] {
                        Some((x, u.hp * self.w * self.h + x))
                    } else {
                        None
                    }
                })
                .min_by_key(|(_, a)| a.clone())
            {
                if let Cell::Unit(u) = self.cells[e].clone() {
                    if u.hp > self.attack[&unit.species] {
                        self.cells[e] = Cell::Unit(Unit {
                            hp: u.hp - self.attack[&unit.species],
                            ..u
                        });
                    } else {
                        self.cells[e] = Cell::Empty;
                        if part2 && (u.species == Species::Elf) {
                            return Some(Species::Goblin);
                        }
                    }
                }
            }
        }
        None
    }

    pub fn hit_sum(&self) -> usize {
        self.cells
            .iter()
            .filter_map(|x| {
                if let Cell::Unit(u) = x {
                    Some(u.hp)
                } else {
                    None
                }
            })
            .sum()
    }
}

#[aoc_generator(day15)]
fn parse(input: &str) -> Box<Grid> {
    Box::new(input.parse().unwrap())
}

#[aoc(day15, part1)]
fn part1(grid: &Grid) -> usize {
    let mut grid = grid.clone();
    println!("{}", grid);
    let mut counter = 0;
    while grid.step(false).is_none() {
        counter += 1;
    }
    println!("{}", grid);
    counter * grid.hit_sum()
}

#[aoc(day15, part2)]
fn part2(grid: &Grid) -> usize {
    let mut attack = HashMap::new();
    attack.insert(Species::Goblin, 3);
    attack.insert(Species::Elf, 3);
    loop {
        {
            let e = attack.entry(Species::Elf).or_insert(0);
            *e += 1;
        }
        let mut grid = Grid {
            attack: attack.clone(),
            ..grid.clone()
        };
        let mut counter = 0;
        loop {
            if let Some(s) = grid.step(true) {
                if s == Species::Elf {
                    println!("{}", grid);
                    return counter * grid.hit_sum();
                }
                break;
            }
            counter += 1;
        }
    }
}
