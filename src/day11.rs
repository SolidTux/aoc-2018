use rayon::prelude::*;

const N: usize = 300;

fn power(x: usize, y: usize, serial: usize) -> i64 {
    let a = (x as i64 + 11) * (y as i64 + 1) + serial as i64;
    let b = (a % 1000) * (x as i64 + 11);
    let c = (b % 1000) / 100;
    c - 5
}

fn grid(serial: usize) -> [[i64; N]; N] {
    let mut grid = [[0; N]; N];
    for x in 0..N {
        for y in 0..N {
            grid[y][x] = power(x, y, serial);
        }
    }
    grid
}

fn block(grid: &[[i64; N]; N], size: usize) -> ((usize, usize), i64) {
    let mut max = ::std::i64::MIN;
    let mut res = (0, 0);
    for x in 0..=(N - size) {
        for y in 0..=(N - size) {
            let mut sum = 0;
            for x1 in x..(x + size) {
                for y1 in y..(y + size) {
                    sum += grid[y1][x1];
                }
            }
            if sum > max {
                max = sum;
                res = (x + 1, y + 1)
            }
        }
    }
    (res, max)
}

#[aoc(day11, part1)]
fn part1(input: &str) -> String {
    let serial = input.trim().parse::<usize>().unwrap();
    let ((x, y), _) = block(&grid(serial), 3);
    format!("{},{}", x, y)
}

#[aoc(day11, part2)]
fn part2(input: &str) -> String {
    let serial = input.trim().parse::<usize>().unwrap();
    let (((x, y), _m), s) = (1 as usize..301)
        .into_par_iter()
        .map(|size| (block(&grid(serial), size), size))
        .reduce(
            || (((0, 0), 0), 0),
            |a, b| {
                if (a.0).1 > (b.0).1 {
                    a
                } else {
                    b
                }
            },
        );
    format!("{},{},{}", x, y, s)
}
