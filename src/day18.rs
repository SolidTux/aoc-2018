use failure::Error;
use std::{fmt, str::FromStr};

#[derive(Clone, PartialEq, Eq)]
enum Cell {
    Open,
    Tree,
    Lumberyard,
}

impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Cell::Open => write!(f, "  "),
            Cell::Tree => write!(f, "🌲"),
            Cell::Lumberyard => write!(f, "❎"),
        }
    }
}

#[derive(Clone)]
struct Grid {
    cells: Vec<Cell>,
    w: usize,
    h: usize,
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0..(self.w * self.h) {
            write!(f, "{}", self.cells[i as usize])?;
            if i % self.w == self.w - 1 {
                writeln!(f)?;
            }
        }
        writeln!(f)
    }
}

impl FromStr for Grid {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let w = s
            .lines()
            .next()
            .ok_or(format_err!("Could not split lines."))?
            .trim()
            .len();
        let h = s.lines().count();
        let cells = s
            .chars()
            .filter_map(|c| match c {
                '.' => Some(Cell::Open),
                '#' => Some(Cell::Lumberyard),
                '|' => Some(Cell::Tree),
                _ => None,
            })
            .collect();
        Ok(Grid { cells, w, h })
    }
}

impl Grid {
    fn step(&mut self) {
        let adjacent: [(i32, i32); 8] = [
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        ];
        self.cells = (0..(self.w * self.h))
            .map(|i| {
                let xi = (i % self.w) as i32;
                let yi = (i / self.w) as i32;
                let (_o, t, l) = adjacent
                    .iter()
                    .filter_map(|&(x, y)| {
                        if (xi + x >= 0)
                            && (xi + x < self.w as i32)
                            && (yi + y >= 0)
                            && (yi + y < self.h as i32)
                        {
                            Some(self.cells[(i as i32 + x + y * self.w as i32) as usize].clone())
                        } else {
                            None
                        }
                    })
                    .fold((0, 0, 0), |acc, c| match c {
                        Cell::Open => (acc.0 + 1, acc.1, acc.2),
                        Cell::Tree => (acc.0, acc.1 + 1, acc.2),
                        Cell::Lumberyard => (acc.0, acc.1, acc.2 + 1),
                    });
                match self.cells[i] {
                    Cell::Open => {
                        if t >= 3 {
                            Cell::Tree
                        } else {
                            Cell::Open
                        }
                    }
                    Cell::Tree => {
                        if l >= 3 {
                            Cell::Lumberyard
                        } else {
                            Cell::Tree
                        }
                    }
                    Cell::Lumberyard => {
                        if (l > 0) && (t > 0) {
                            Cell::Lumberyard
                        } else {
                            Cell::Open
                        }
                    }
                }
            })
            .collect::<Vec<_>>();
    }

    pub fn count(&self, cell: Cell) -> usize {
        self.cells.iter().filter(|&x| *x == cell).count()
    }
}

#[aoc_generator(day18)]
fn parse(input: &str) -> Box<Grid> {
    Box::new(input.parse().unwrap())
}

#[aoc(day18, part1)]
fn part1(grid: &Grid) -> usize {
    let mut grid = grid.clone();
    println!("{}", grid);
    for _ in 0..10 {
        grid.step();
    }
    println!("{}", grid);
    grid.count(Cell::Tree) * grid.count(Cell::Lumberyard)
}

#[aoc(day18, part2)]
fn part2(grid: &Grid) -> usize {
    let mut grid = grid.clone();
    println!("{}", grid);
    for _ in 0..(532 + 1000000000_u64 % 28) {
        grid.step();
    }
    println!("{}", grid);
    grid.count(Cell::Tree) * grid.count(Cell::Lumberyard)
}
