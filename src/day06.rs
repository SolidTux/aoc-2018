#[aoc_generator(day6)]
fn coords(input: &str) -> Vec<(usize, usize)> {
    input
        .lines()
        .map(|l| {
            let pos = l
                .split(",")
                .map(|x| x.trim().parse().unwrap())
                .collect::<Vec<_>>();
            (pos[0], pos[1])
        })
        .collect::<Vec<_>>()
}

fn hamming(a: (usize, usize), b: (usize, usize)) -> usize {
    ((b.0 as isize - a.0 as isize).abs() + (b.1 as isize - a.1 as isize).abs()) as usize
}

#[aoc(day6, part1)]
fn part1(input: &Vec<(usize, usize)>) -> usize {
    let (x0, y0, x1, y1) = input
        .iter()
        .fold((std::usize::MAX, std::usize::MAX, 0, 0), |acc, (x, y)| {
            (acc.0.min(*x), acc.1.min(*y), acc.2.max(*x), acc.3.max(*y))
        });
    println!("{} {} {} {}", x0, y0, x1, y1);
    let mut grid = Vec::new();
    for x in x0..=x1 {
        for y in y0..=y1 {
            grid.push(
                input
                    .iter()
                    .min_by(|&&a, &&b| hamming(a, (x, y)).cmp(&hamming(b, (x, y))))
                    .unwrap()
                    .clone(),
            );
        }
    }
    input
        .iter()
        .map(|&p| grid.iter().filter(|&&x| x == p).count())
        .max()
        .unwrap()
}
