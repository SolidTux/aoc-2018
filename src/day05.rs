fn clean(chars: &[char]) -> Vec<char> {
    let mut res = Vec::new();
    let mut last = ' ';
    for c in chars {
        if (*c == last) || (c.to_ascii_uppercase() != last.to_ascii_uppercase()) {
            res.push(*c);
            last = *c;
        } else {
            res.pop();
            last = ' ';
        }
    }
    res
}

#[aoc(day5, part1)]
fn part1(input: &str) -> usize {
    let mut chars = input.trim().chars().collect::<Vec<_>>();
    let mut last_len = 0;
    while chars.len() != last_len {
        last_len = chars.len();
        chars = clean(&chars);
    }
    chars.len()
}

#[aoc(day5, part2)]
fn part2(input: &str) -> usize {
    let full_chars = input.trim().chars().collect::<Vec<_>>();
    "abcdefghijklmnopqrstuvwxyz"
        .chars()
        .map(|c| {
            let mut chars = full_chars
                .iter()
                .filter(|&x| x.to_ascii_lowercase() != c)
                .cloned()
                .collect::<Vec<char>>();
            let mut last_len = 0;
            while chars.len() != last_len {
                last_len = chars.len();
                chars = clean(&chars);
            }
            chars.len()
        }).min()
        .unwrap()
}
