use image::{ImageBuffer, Luma};
use regex::Regex;
use std::{
    fs,
    i64::{MAX, MIN},
};

#[derive(Clone, Debug)]
struct Light {
    position: (i64, i64),
    velocity: (i64, i64),
}

impl Light {
    pub fn step(&mut self) {
        self.position.0 += self.velocity.0;
        self.position.1 += self.velocity.1;
    }
}

fn render(lights: &Vec<Light>, filename: &str) {
    let (minx, miny, maxx, maxy) = lights.iter().fold((MAX, MAX, MIN, MIN), |acc, l| {
        (
            acc.0.min(l.position.0),
            acc.1.min(l.position.1),
            acc.2.max(l.position.0),
            acc.3.max(l.position.1),
        )
    });
    let w = (maxx - minx + 1) as u32;
    let h = (maxy - miny + 1) as u32;
    let mut image: ImageBuffer<Luma<u8>, _> = ImageBuffer::new(w, h);
    for l in lights {
        let x = (l.position.0 - minx) as u32;
        let y = (l.position.1 - miny) as u32;
        image.put_pixel(x, y, Luma([255]));
    }
    image.save(filename).unwrap();
}

#[aoc_generator(day10)]
fn parse(input: &str) -> Vec<Light> {
    let re =
        Regex::new(r"position=<\s*([0-9-]*),\s*([0-9-]*)> velocity=<\s*([0-9-]*),\s*([0-9-]*)>")
            .unwrap();
    re.captures_iter(input)
        .map(|c| {
            let px = c[1].parse().unwrap();
            let py = c[2].parse().unwrap();
            let vx = c[3].parse().unwrap();
            let vy = c[4].parse().unwrap();
            Light {
                position: (px, py),
                velocity: (vx, vy),
            }
        })
        .collect()
}

#[aoc(day10, part1)]
fn part1(input: &Vec<Light>) -> String {
    let mut lights: Vec<Light> = (*input).clone();

    let _ = fs::create_dir(format!("{}/output", env!("CARGO_MANIFEST_DIR")));

    for i in 0..100_000 {
        for l in lights.iter_mut() {
            l.step();
        }
        let (minx, miny, maxx, maxy) = lights.iter().fold((MAX, MAX, MIN, MIN), |acc, l| {
            (
                acc.0.min(l.position.0),
                acc.1.min(l.position.1),
                acc.2.max(l.position.0),
                acc.3.max(l.position.1),
            )
        });
        let w = (maxx - minx + 1) as u32;
        let h = (maxy - miny + 1) as u32;
        //println!("{:10}: {:10}x{:10}", i, w, h);
        if (w < 1000) && (h < 1000) {
            render(
                &lights,
                &format!("{}/output/{:06}.png", env!("CARGO_MANIFEST_DIR"), i),
            );
        }
    }
    String::new()
}
