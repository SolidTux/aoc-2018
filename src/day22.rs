use regex::Regex;
use std::collections::{HashMap, HashSet};

#[aoc_generator(day22)]
fn parse(input: &str) -> Box<(usize, (usize, usize))> {
    let re_dep = Regex::new(r"depth: (\d*)").unwrap();
    let re_tar = Regex::new(r"target: (\d*),(\d*)").unwrap();
    let dep = re_dep
        .captures(input)
        .unwrap()
        .get(1)
        .unwrap()
        .as_str()
        .parse()
        .unwrap();
    let cap_tar = re_tar.captures(input).unwrap();
    let x = cap_tar.get(1).unwrap().as_str().parse().unwrap();
    let y = cap_tar.get(2).unwrap().as_str().parse().unwrap();
    Box::new((dep, (x, y)))
}

fn regions(dep: usize, tar: (usize, usize)) -> HashMap<(usize, usize), usize> {
    let (x, y) = tar;
    let mut geo = HashMap::new();
    geo.insert((0, 0), 0);
    for x1 in 0..=x {
        geo.insert((x1, 0), x1 * 16807);
    }
    for y1 in 1..=y {
        geo.insert((0, y1), y1 * 48271);
        for x1 in 1..=x {
            let a = (geo.get(&(x1 - 1, y1)).unwrap().clone() + dep) % 20183;
            let b = (geo.get(&(x1, y1 - 1)).unwrap().clone() + dep) % 20183;
            geo.insert((x1, y1), a * b);
        }
    }
    let ero = geo
        .iter()
        .map(|(&k, &v)| (k, (v + dep) % 20183))
        .collect::<HashMap<_, _>>();
    let reg = ero
        .iter()
        .map(|(&k, &v)| (k, v % 3))
        .collect::<HashMap<_, _>>();
    reg
}

#[aoc(day22, part1)]
fn part1(input: &(usize, (usize, usize))) -> usize {
    let &(dep, (x, y)) = input;
    let reg = regions(dep, (x, y));
    reg.values().sum::<usize>() - reg[&(x, y)]
}

enum Tool {
    Nothing,
    Torch,
    Climbing,
}

enum Region {
    Rocky,
    Wet,
    Narrow,
}

#[aoc(day22, part2)]
fn part2(input: &(usize, (usize, usize))) -> usize {
    let &(dep, (tx, ty)) = input;
    let reg = regions(dep, (tx, ty))
        .iter()
        .filter_map(|(&k, &v)| match v {
            0 => Some((k, Region::Rocky)),
            1 => Some((k, Region::Wet)),
            2 => Some((k, Region::Narrow)),
            _ => None,
        })
        .collect::<HashMap<_, _>>();
    let mut visited = HashSet::new();
    let mut paths = vec![((0, 0), 0, Tool::Torch)];
    loop {
        let mut next = Vec::new();
        for p in &paths {
            let &((x, y), dur, ref tool) = p;
            for p2 in &[(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)] {
                let p2 = p2.clone();
                if !visited.contains(&p2) {
                    if let Some(r) = reg.get(&(x, y)) {
                        if let Some(r2) = reg.get(&p2) {
                            match (r, tool) {
                                (Region::Rocky, Tool::Climbing) => {
                                    visited.insert(p2);
                                    match r2 {
                                        Region::Rocky => {
                                            next.push((p2, dur + 1, Tool::Climbing));
                                            next.push((p2, dur + 8, Tool::Torch));
                                        }
                                        Region::Wet => {}
                                        Region::Narrow => {}
                                    }
                                }
                                (Region::Rocky, Tool::Torch) => {}
                                (Region::Wet, Tool::Climbing) => {}
                                (Region::Wet, Tool::Nothing) => {}
                                (Region::Narrow, Tool::Nothing) => {}
                                (Region::Narrow, Tool::Torch) => {}
                                _ => unreachable!(),
                            }
                        }
                    }
                }
            }
        }
        paths = next;
    }
}
