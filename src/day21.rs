use regex::Regex;
use std::{collections::HashSet, str::FromStr};

const REGS: usize = 6;

struct Cpu {
    pub registers: [usize; REGS],
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            registers: [0; REGS],
        }
    }

    pub fn execute(&mut self, instr: &Instruction, data: &[usize]) {
        match instr {
            Instruction::Addr => {
                self.registers[data[2]] = self.registers[data[0]] + self.registers[data[1]]
            }
            Instruction::Addi => self.registers[data[2]] = self.registers[data[0]] + data[1],
            Instruction::Mulr => {
                self.registers[data[2]] = self.registers[data[0]] * self.registers[data[1]]
            }
            Instruction::Muli => self.registers[data[2]] = self.registers[data[0]] * data[1],
            Instruction::Banr => {
                self.registers[data[2]] = self.registers[data[0]] & self.registers[data[1]]
            }
            Instruction::Bani => self.registers[data[2]] = self.registers[data[0]] & data[1],
            Instruction::Borr => {
                self.registers[data[2]] = self.registers[data[0]] | self.registers[data[1]]
            }
            Instruction::Bori => self.registers[data[2]] = self.registers[data[0]] | data[1],
            Instruction::Setr => self.registers[data[2]] = self.registers[data[0]],
            Instruction::Seti => self.registers[data[2]] = data[0],
            Instruction::Gtir => {
                if data[0] > self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Gtri => {
                if self.registers[data[0]] > data[1] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Gtrr => {
                if self.registers[data[0]] > self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Eqir => {
                if data[0] == self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Eqri => {
                if self.registers[data[0]] == data[1] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Eqrr => {
                if self.registers[data[0]] == self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
        }
    }
}

#[derive(Hash, PartialEq, Eq, Debug, Clone)]
enum Instruction {
    Addr,
    Addi,
    Mulr,
    Muli,
    Banr,
    Bani,
    Borr,
    Bori,
    Setr,
    Seti,
    Gtir,
    Gtri,
    Gtrr,
    Eqir,
    Eqri,
    Eqrr,
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "addr" => Ok(Instruction::Addr),
            "addi" => Ok(Instruction::Addi),
            "mulr" => Ok(Instruction::Mulr),
            "muli" => Ok(Instruction::Muli),
            "banr" => Ok(Instruction::Banr),
            "bani" => Ok(Instruction::Bani),
            "borr" => Ok(Instruction::Borr),
            "bori" => Ok(Instruction::Bori),
            "setr" => Ok(Instruction::Setr),
            "seti" => Ok(Instruction::Seti),
            "gtir" => Ok(Instruction::Gtir),
            "gtri" => Ok(Instruction::Gtri),
            "gtrr" => Ok(Instruction::Gtrr),
            "eqir" => Ok(Instruction::Eqir),
            "eqri" => Ok(Instruction::Eqri),
            "eqrr" => Ok(Instruction::Eqrr),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone)]
struct Program {
    instructions: Vec<(Instruction, [usize; 3])>,
    ip_reg: usize,
}

impl Program {
    pub fn max_start(&mut self, cpu: &mut Cpu) -> Option<usize> {
        let mut ip = 0;
        let mut seen = HashSet::new();
        let mut last = 0;
        while ip < self.instructions.len() {
            cpu.registers[self.ip_reg] = ip;
            let (instr, data) = self.instructions[ip].clone();
            cpu.execute(&instr, &data);
            if instr == Instruction::Eqrr && data[1] == 0 {
                let x = cpu.registers[data[0]];
                //println!("{:>20}", x);
                if seen.contains(&x) {
                    return Some(last);
                } else {
                    last = x;
                    seen.insert(x);
                }
            }
            ip = cpu.registers[self.ip_reg];
            ip += 1;
        }
        None
    }

    pub fn min_start(&mut self, cpu: &mut Cpu) -> Option<usize> {
        let mut ip = 0;
        while ip < self.instructions.len() {
            cpu.registers[self.ip_reg] = ip;
            let (instr, data) = self.instructions[ip].clone();
            cpu.execute(&instr, &data);
            if instr == Instruction::Eqrr && data[1] == 0 {
                return Some(cpu.registers[data[0]]);
            }
            ip = cpu.registers[self.ip_reg];
            ip += 1;
        }
        None
    }
}

#[aoc_generator(day21)]
fn parse(input: &str) -> Box<Program> {
    let re_ip = Regex::new(r"#ip (\d*)").unwrap();
    let ip_reg = re_ip
        .captures(input)
        .unwrap()
        .get(1)
        .unwrap()
        .as_str()
        .parse()
        .unwrap();
    let re = Regex::new(r"(\w*) (\d*) (\d*) (\d*)").unwrap();
    let instructions = re
        .captures_iter(input)
        .map(|c| {
            (
                c[1].parse().unwrap(),
                [
                    c[2].parse().unwrap(),
                    c[3].parse().unwrap(),
                    c[4].parse().unwrap(),
                ],
            )
        })
        .collect();
    Box::new(Program {
        instructions,
        ip_reg,
    })
}

#[aoc(day21, part1)]
fn part1(prog: &Program) -> usize {
    let mut prog = prog.clone();
    let mut cpu = Cpu::new();
    prog.min_start(&mut cpu).unwrap()
}

#[aoc(day21, part2)]
fn part2(prog: &Program) -> usize {
    let mut prog = prog.clone();
    let mut cpu = Cpu::new();
    prog.max_start(&mut cpu).unwrap()
}
