use regex::Regex;

#[aoc_generator(day9)]
fn parse(input: &str) -> Box<(usize, usize)> {
    let re = Regex::new(r"(\d*) players; last marble is worth (\d*) points").unwrap();
    let c = re.captures(input).unwrap();
    Box::new((c[1].parse().unwrap(), c[2].parse().unwrap()))
}
#[aoc(day9, part1)]
fn part1(input: &(usize, usize)) -> usize {
    solve(input.0, input.1)
}

#[aoc(day9, part2)]
fn part2(input: &(usize, usize)) -> usize {
    solve(input.0, 100 * input.1)
}

struct Node {
    value: usize,
    prev: usize,
    next: usize,
}

fn solve(players: usize, size: usize) -> usize {
    let mut nodes = Vec::with_capacity(size);
    nodes.push(Node {
        value: 0,
        prev: 0,
        next: 0,
    });
    let mut points = vec![0; players];
    let mut active = 0;
    let mut player = 0;
    for i in 1..=size {
        if i % 23 == 0 {
            points[player] += i;
            for _ in 0..7 {
                active = nodes[active].prev;
            }
            points[player] += nodes[active].value;
            let next = nodes[active].next;
            let prev = nodes[active].prev;
            nodes[prev].next = next;
            nodes[next].prev = prev;
            active = nodes[active].next;
        } else {
            let node = Node {
                value: i,
                prev: nodes[active].next,
                next: nodes[nodes[active].next].next,
            };
            nodes[node.prev].next = nodes.len();
            nodes[node.next].prev = nodes.len();
            active = nodes.len();
            nodes.push(node);
        }
        player = (player + 1) % points.len();
    }
    *(points.iter().max().unwrap())
}
