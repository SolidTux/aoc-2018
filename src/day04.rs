use regex::Regex;
use std::collections::HashMap;

#[derive(Debug, Clone)]
struct Event {
    time: Time,
    event_type: EventType,
}

#[derive(Debug, Clone)]
struct Time {
    year: usize,
    month: usize,
    day: usize,
    hour: usize,
    minute: usize,
}

#[derive(Debug, Clone)]
enum EventType {
    Start(usize),
    Awake,
    Sleep,
}

#[aoc_generator(day4)]
fn parse(input: &str) -> Vec<Event> {
    let re = Regex::new(r"\[(\d*)\-(\d*)\-(\d*) (\d*):(\d*)\] (.*)").unwrap();
    let re_start = Regex::new(r"#(\d*)").unwrap();
    re.captures_iter(input)
        .map(|c| {
            let t = if c[6].contains("begins") {
                EventType::Start(
                    re_start
                        .captures(&c[6])
                        .unwrap()
                        .get(1)
                        .unwrap()
                        .as_str()
                        .parse()
                        .unwrap(),
                )
            } else if c[6].contains("asleep") {
                EventType::Sleep
            } else if c[6].contains("wakes") {
                EventType::Awake
            } else {
                unimplemented!()
            };
            Event {
                time: Time {
                    year: c[1].parse().unwrap(),
                    month: c[2].parse().unwrap(),
                    day: c[3].parse().unwrap(),
                    hour: c[4].parse().unwrap(),
                    minute: c[5].parse().unwrap(),
                },
                event_type: t,
            }
        }).collect()
}

#[aoc(day4, part1)]
fn part1(input: &Vec<Event>) -> usize {
    let mut minutes = HashMap::new();
    let mut current = 0;
    let mut sleep_start = 0;
    let mut data = (*input).clone();
    data.sort_by_key(|e| {
        e.time.minute
            + 60 * (e.time.hour + 24 * (e.time.day + 31 * (e.time.month + 12 * e.time.year)))
    });
    for e in data {
        match e.event_type {
            EventType::Start(id) => current = id,
            EventType::Sleep => sleep_start = e.time.minute,
            EventType::Awake => {
                if current > 0 {
                    let data = minutes.entry(current).or_insert([0; 60]);
                    for i in sleep_start..e.time.minute {
                        (*data)[i] += 1;
                    }
                }
            }
        }
    }
    let (&id, _) = minutes
        .iter()
        .max_by_key(|(_, v)| v.iter().sum::<usize>())
        .unwrap();
    let min = minutes[&id]
        .iter()
        .position(|x| x == minutes[&id].iter().max().unwrap())
        .unwrap();
    println!("{} {}", id, min);
    id * min
}

#[aoc(day4, part2)]
fn part2(input: &Vec<Event>) -> usize {
    let mut minutes = HashMap::new();
    let mut current = 0;
    let mut sleep_start = 0;
    let mut data = (*input).clone();
    data.sort_by_key(|e| {
        e.time.minute
            + 60 * (e.time.hour + 24 * (e.time.day + 31 * (e.time.month + 12 * e.time.year)))
    });
    for e in data {
        match e.event_type {
            EventType::Start(id) => current = id,
            EventType::Sleep => sleep_start = e.time.minute,
            EventType::Awake => {
                if current > 0 {
                    let data = minutes.entry(current).or_insert([0; 60]);
                    for i in sleep_start..e.time.minute {
                        (*data)[i] += 1;
                    }
                }
            }
        }
    }
    let (id, min, _) = minutes
        .iter()
        .map(|(id, e)| {
            let &m = e.iter().max().unwrap();
            (id, e.iter().position(|&x| x == m).unwrap(), m)
        }).max_by_key(|(_, _, x)| x.clone())
        .unwrap();
    id * min
}
