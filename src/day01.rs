use std::collections::HashSet;

#[aoc(day1, part1, Sum)]
fn part1_sum(input: &str) -> isize {
    input.lines().map(|x| x.parse::<isize>().unwrap()).sum()
}

#[aoc(day1, part1, ForLoop)]
fn part1_for(input: &str) -> isize {
    let mut freq = 0;
    for x in input.lines().map(|x| x.parse::<isize>().unwrap()) {
        freq += x;
    }
    freq
}

#[aoc(day1, part2, HashSet)]
fn part2_set(input: &str) -> isize {
    let mut visited = HashSet::new();
    let mut freq = 0;
    for x in input.lines().cycle().map(|x| x.parse::<isize>().unwrap()) {
        visited.insert(freq);
        freq += x;
        if visited.contains(&freq) {
            return freq;
        }
    }
    unreachable!()
}
