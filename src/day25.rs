use regex::Regex;
use std::collections::{HashMap, HashSet};

#[aoc_generator(day25)]
fn parse(input: &str) -> Vec<[isize; 4]> {
    let re = Regex::new(r"([-\d]*),([-\d]*),([-\d*]*),([-\d*]*)").unwrap();
    re.captures_iter(input)
        .map(|c| {
            [
                c[1].parse().unwrap(),
                c[2].parse().unwrap(),
                c[3].parse().unwrap(),
                c[4].parse().unwrap(),
            ]
        })
        .collect()
}

fn manhattan(a: &[isize], b: &[isize]) -> usize {
    a.iter()
        .zip(b.iter())
        .map(|(&a, &b)| (a - b).abs() as usize)
        .sum()
}

fn constellations(points: &[[isize; 4]]) -> Vec<HashSet<usize>> {
    let mut tmp = HashMap::new();
    for i in 0..points.len() {
        for j in (i + 1)..points.len() {
            if manhattan(&points[i], &points[j]) <= 3 {
                {
                    let ei = tmp.entry(i).or_insert(HashSet::new());
                    (*ei).insert(j);
                }
                {
                    let ej = tmp.entry(j).or_insert(HashSet::new());
                    (*ej).insert(i);
                }
            }
        }
    }
    let mut finished = false;
    while !finished {
        finished = true;
        let mut next = HashMap::new();
        let mut delete = Vec::new();
        'tmp: for (k, v) in &tmp {
            if finished {
                let mut nv = v.clone();
                for x in v {
                    if let Some(y) = tmp.get(x) {
                        nv = nv.union(&y).cloned().collect();
                        nv.remove(k);
                        finished = false;
                        delete.push(*x);
                        next.insert(*k, nv);
                        continue 'tmp;
                    }
                }
                next.insert(*k, nv);
            } else {
                next.insert(*k, v.clone());
            }
        }
        for x in delete {
            next.remove(&x);
        }
        tmp = next;
    }
    let mut res = Vec::new();
    for (k, v) in tmp {
        let mut s = v.clone();
        s.insert(k);
        res.push(s);
    }
    res
}

#[aoc(day25, part1)]
fn part1(input: &[[isize; 4]]) -> usize {
    println!("{:?}", input);
    let con = constellations(input);
    for c in &con {
        println!("{:?}", c);
    }
    con.len()
}
