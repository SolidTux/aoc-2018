use regex::Regex;
use std::collections::VecDeque;

type Plant = bool;
type Plants = VecDeque<Plant>;
type Rule = (Plants, Plant);
type Rules = Vec<Rule>;

fn plant_count(plants: &Plants, start: i64) -> i64 {
    plants
        .iter()
        .zip(start..)
        .filter_map(|(&p, i)| if p { Some(i) } else { None })
        .sum()
}

#[aoc_generator(day12)]
fn parse(input: &str) -> Box<(Plants, Rules)> {
    let re_plants = Regex::new(r"initial state: ([\.#]*)").unwrap();
    let re_rules = Regex::new(r"(.....) => (.)").unwrap();

    let plants = re_plants
        .captures(input)
        .unwrap()
        .get(1)
        .unwrap()
        .as_str()
        .chars()
        .map(|x| x == '#')
        .collect();

    let rules = re_rules
        .captures_iter(input)
        .map(|c| (c[1].chars().map(|x| x == '#').collect(), &c[2] == "#"))
        .collect();

    Box::new((plants, rules))
}

fn start_trim(plants: &Plants) -> bool {
    for i in 0..5 {
        if plants[i] {
            return false;
        }
    }
    true
}

fn end_trim(plants: &Plants) -> bool {
    for i in (plants.len() - 5)..(plants.len()) {
        if plants[i] {
            return false;
        }
    }
    true
}

#[aoc(day12, part1)]
fn part1(input: &(Plants, Rules)) -> i64 {
    let (plants, rules) = input;
    let mut plants = plants.clone();
    let mut start = 0;
    for _ in 0..20 {
        for _ in 0..4 {
            plants.push_front(false);
            plants.push_back(false);
        }
        start -= 4;
        let mut next = ::std::iter::repeat(false)
            .take(plants.len())
            .collect::<Plants>();

        for j in 2..(next.len() - 2) {
            'outer: for (pattern, plant) in rules {
                for k in (j - 2)..=(j + 2) {
                    if plants[k] != pattern[k - j + 2] {
                        continue 'outer;
                    }
                }
                next[j] = *plant;
                break;
            }
        }

        if start_trim(&plants) {
            while !next[4] {
                next.pop_front();
                start += 1;
            }
        }

        if end_trim(&plants) {
            while !next[next.len() - 4] {
                next.pop_back();
            }
        }

        plants = next;
    }
    plant_count(&plants, start)
}

#[aoc(day12, part2)]
fn part2(input: &(Plants, Rules)) -> i64 {
    let (plants, rules) = input;
    let mut plants = plants.clone();
    let mut start = 0;
    for _ in 0..1000 {
        for _ in 0..4 {
            plants.push_front(false);
            plants.push_back(false);
        }
        start -= 4;
        let mut next = ::std::iter::repeat(false)
            .take(plants.len())
            .collect::<Plants>();

        for j in 2..(next.len() - 2) {
            'outer: for (pattern, plant) in rules {
                for k in (j - 2)..=(j + 2) {
                    if plants[k] != pattern[k - j + 2] {
                        continue 'outer;
                    }
                }
                next[j] = *plant;
                break;
            }
        }

        if start_trim(&plants) {
            while !next[4] {
                next.pop_front();
                start += 1;
            }
        }

        if end_trim(&plants) {
            while !next[next.len() - 5] {
                next.pop_back();
            }
        }

        if plants == next {
            break;
        }

        plants = next;
    }
    plant_count(&plants, 50_000_000_000 - 1000 + start)
}
