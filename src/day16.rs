use regex::Regex;
use std::collections::{HashMap, HashSet};

struct Cpu {
    pub registers: [usize; 4],
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu { registers: [0; 4] }
    }

    pub fn execute(&mut self, instr: &Instruction, data: &[usize]) {
        match instr {
            Instruction::Addr => {
                self.registers[data[2]] = self.registers[data[0]] + self.registers[data[1]]
            }
            Instruction::Addi => self.registers[data[2]] = self.registers[data[0]] + data[1],
            Instruction::Mulr => {
                self.registers[data[2]] = self.registers[data[0]] * self.registers[data[1]]
            }
            Instruction::Muli => self.registers[data[2]] = self.registers[data[0]] * data[1],
            Instruction::Banr => {
                self.registers[data[2]] = self.registers[data[0]] & self.registers[data[1]]
            }
            Instruction::Bani => self.registers[data[2]] = self.registers[data[0]] & data[1],
            Instruction::Borr => {
                self.registers[data[2]] = self.registers[data[0]] | self.registers[data[1]]
            }
            Instruction::Bori => self.registers[data[2]] = self.registers[data[0]] | data[1],
            Instruction::Setr => self.registers[data[2]] = self.registers[data[0]],
            Instruction::Seti => self.registers[data[2]] = data[0],
            Instruction::Gtir => {
                if data[0] > self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Gtri => {
                if self.registers[data[0]] > data[1] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Gtrr => {
                if self.registers[data[0]] > self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Eqir => {
                if data[0] == self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Eqri => {
                if self.registers[data[0]] == data[1] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
            Instruction::Eqrr => {
                if self.registers[data[0]] == self.registers[data[1]] {
                    self.registers[data[2]] = 1;
                } else {
                    self.registers[data[2]] = 0;
                }
            }
        }
    }
}

#[derive(Hash, PartialEq, Eq, Debug, Clone)]
enum Instruction {
    Addr,
    Addi,
    Mulr,
    Muli,
    Banr,
    Bani,
    Borr,
    Bori,
    Setr,
    Seti,
    Gtir,
    Gtri,
    Gtrr,
    Eqir,
    Eqri,
    Eqrr,
}

impl Instruction {
    pub fn all() -> HashSet<Instruction> {
        use self::Instruction::*;
        let mut res = HashSet::new();
        res.insert(Addr);
        res.insert(Addi);
        res.insert(Mulr);
        res.insert(Muli);
        res.insert(Banr);
        res.insert(Bani);
        res.insert(Borr);
        res.insert(Bori);
        res.insert(Setr);
        res.insert(Seti);
        res.insert(Gtir);
        res.insert(Gtri);
        res.insert(Gtrr);
        res.insert(Eqir);
        res.insert(Eqri);
        res.insert(Eqrr);
        res
    }
}

#[derive(Debug)]
struct Example {
    before: [usize; 4],
    after: [usize; 4],
    instruction: [usize; 4],
}

impl Example {
    pub fn instructions(&self) -> HashSet<Instruction> {
        let mut cpu = Cpu::new();
        let mut res = HashSet::new();
        for i in Instruction::all() {
            cpu.registers = self.before;
            cpu.execute(&i, &self.instruction[1..]);
            if cpu.registers == self.after {
                res.insert(i);
            }
        }
        res
    }
}

#[aoc_generator(day16)]
fn parse(input: &str) -> Box<(Vec<Example>, Vec<[usize; 4]>)> {
    let mut lines = input.lines();
    let mut res = Vec::new();
    let mut program = Vec::new();
    let re_before = Regex::new(r"Before:\s*\[(\d*), (\d*), (\d*), (\d*)\]").unwrap();
    let re_inst = Regex::new(r"(\d*) (\d*) (\d*) (\d*)").unwrap();
    let re_after = Regex::new(r"After:\s*\[(\d*), (\d*), (\d*), (\d*)\]").unwrap();
    while let Some(line) = lines.next() {
        if let Some(c) = re_before.captures(line) {
            let mut before = [0; 4];
            let mut instruction = [0; 4];
            let mut after = [0; 4];
            let ci = re_inst.captures(lines.next().unwrap()).unwrap();
            let ca = re_after.captures(lines.next().unwrap()).unwrap();
            for i in 0..4 {
                before[i] = c[i + 1].parse().unwrap();
                instruction[i] = ci[i + 1].parse().unwrap();
                after[i] = ca[i + 1].parse().unwrap();
            }
            res.push(Example {
                before,
                after,
                instruction,
            });
        } else if let Some(c) = re_inst.captures(line) {
            let mut instruction = [0; 4];
            for i in 0..4 {
                instruction[i] = c[i + 1].parse().unwrap();
            }
            program.push(instruction);
        }
    }
    Box::new((res, program))
}

#[aoc(day16, part1)]
fn part1(input: &(Vec<Example>, Vec<[usize; 4]>)) -> usize {
    input
        .0
        .iter()
        .map(|x| x.instructions().len())
        .filter(|&x| x >= 3)
        .count()
}

#[aoc(day16, part2)]
fn part2(input: &(Vec<Example>, Vec<[usize; 4]>)) -> usize {
    let mut opcodes = HashMap::new();
    let mut map = HashMap::new();
    for e in &input.0 {
        let instr = e.instructions();
        let entry = opcodes
            .entry(e.instruction[0])
            .or_insert(Instruction::all());
        *entry = (*entry).intersection(&instr).cloned().collect();
    }
    while !opcodes.is_empty() {
        let mut delete = HashSet::new();
        let mut delete_op = HashSet::new();
        for (&o, ref i) in &opcodes {
            if i.len() == 1 {
                let instr = i.iter().next().unwrap();
                map.insert(o, instr.clone());
                delete_op.insert(o);
                delete.insert(instr.clone());
            }
        }
        for o in delete_op {
            opcodes.remove(&o);
        }
        let mut empty = HashSet::new();
        for i in delete {
            for (instr, v) in opcodes.iter_mut() {
                v.remove(&i);
                if v.is_empty() {
                    empty.insert(*instr);
                }
            }
        }
        for o in empty {
            opcodes.remove(&o);
        }
    }
    let mut cpu = Cpu::new();
    for line in &input.1 {
        cpu.execute(&map[&line[0]], &line[1..]);
    }
    cpu.registers[0]
}
