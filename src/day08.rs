use std::collections::VecDeque;

#[derive(Debug)]
struct Node {
    childs: Vec<Box<Node>>,
    metadata: Vec<usize>,
}

impl Node {
    pub fn metadata_sum(&self) -> usize {
        self.metadata.iter().sum::<usize>()
            + self.childs.iter().map(|x| x.metadata_sum()).sum::<usize>()
    }

    pub fn value(&self) -> usize {
        if self.childs.len() == 0 {
            self.metadata.iter().sum::<usize>()
        } else {
            self.metadata
                .iter()
                .map(|x| {
                    if (x > &0) && (x <= &self.childs.len()) {
                        self.childs[x - 1].value()
                    } else {
                        0
                    }
                })
                .sum::<usize>()
        }
    }
}

fn get_node(data: &mut VecDeque<usize>) -> Option<Node> {
    let nc = data.pop_front()?;
    let nm = data.pop_front()?;
    let mut metadata = Vec::new();
    let mut childs = Vec::new();
    for _ in 0..nc {
        childs.push(Box::new(get_node(data)?));
    }
    for _ in 0..nm {
        metadata.push(data.pop_front()?);
    }
    Some(Node { childs, metadata })
}

#[aoc_generator(day8)]
fn nodes(input: &str) -> Box<Node> {
    let mut data = input
        .split(" ")
        .map(|x| x.trim().parse::<usize>().unwrap())
        .collect::<VecDeque<_>>();
    Box::new(get_node(&mut data).unwrap())
}

#[aoc(day8, part1)]
fn part1(input: &Node) -> usize {
    input.metadata_sum()
}

#[aoc(day8, part2)]
fn part2(input: &Node) -> usize {
    input.value()
}
