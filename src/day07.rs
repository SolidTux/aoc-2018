use regex::Regex;
use std::collections::{HashMap, HashSet};

type Map = HashMap<char, HashSet<char>>;

#[aoc_generator(day7)]
fn parse(input: &str) -> Vec<(char, char)> {
    let re = Regex::new(r"Step (.) must be finished before step (.) can begin\.").unwrap();
    re.captures_iter(input)
        .map(|c| (c[1].chars().next().unwrap(), c[2].chars().next().unwrap()))
        .collect()
}

fn map(input: &Vec<(char, char)>) -> Map {
    let mut map: Map = HashMap::new();
    for &(a, b) in input {
        map.entry(a).or_insert(HashSet::new());
        let entry = map.entry(b).or_insert(HashSet::new());
        (*entry).insert(a);
    }
    map
}

fn next(map: &Map) -> Option<char> {
    let mut available = map
        .iter()
        .filter_map(|(k, v)| if v.is_empty() { Some(k) } else { None })
        .collect::<Vec<_>>();
    available.sort_unstable();
    available.get(0).cloned().cloned()
}

#[aoc(day7, part1)]
fn part1(input: &Vec<(char, char)>) -> String {
    let mut map = map(input);
    let mut res = String::new();
    while let Some(next) = next(&map) {
        res.push(next);
        {
            map.remove(&next);
        }
        for v in map.values_mut() {
            v.remove(&next);
        }
    }
    res
}

#[aoc(day7, part2)]
fn part2(input: &Vec<(char, char)>) -> usize {
    let mut map = map(input);
    let mut workers: Vec<Option<(char, u8)>> = vec![None; 5];
    let mut time = 0;
    while !map.is_empty() || workers.iter().any(|x| !x.is_none()) {
        for w in workers.iter_mut() {
            if let Some((c, t)) = w.clone() {
                if t == 0 {
                    for v in map.values_mut() {
                        v.remove(&c);
                    }
                    *w = None;
                } else {
                    *w = Some((c, t - 1))
                }
            }
            if w.is_none() {
                if let Some(n) = next(&map) {
                    {
                        map.remove(&n);
                    }
                    *w = Some((n, n as u8 - 5));
                }
            }
        }
        //println!(
        //"{:10} {} {} {:?} {:?}",
        //map.is_empty(),
        //workers.iter().any(|x| x.is_none()),
        //time,
        //workers.iter().filter_map(|&x| x).collect::<Vec<_>>(),
        //map.keys()
        //);
        time += 1;
    }
    time - 2
}
