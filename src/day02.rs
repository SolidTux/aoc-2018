#[aoc(day2, part1)]
fn part1(input: &str) -> usize {
    let (a, b) = input.lines().fold((0, 0), |(a, b), x| {
        let mut counter = [0; 26];
        let (mut resa, mut resb) = (a, b);
        for c in x.chars() {
            let ind = (c as usize) - 97;
            if ind < 26 {
                counter[ind] += 1;
            }
        }
        if counter.iter().any(|&x| x == 2) {
            resa += 1;
        }
        if counter.iter().any(|&x| x == 3) {
            resb += 1;
        }
        (resa, resb)
    });
    a * b
}

#[aoc(day2, part2)]
fn part2(input: &str) -> String {
    let lines = input.lines().collect::<Vec<_>>();
    for a in 0..(lines.len() - 1) {
        for b in (a + 1)..lines.len() {
            if lines[b]
                .chars()
                .zip(lines[a].chars())
                .map(|(a, b)| if a == b { 0 } else { 1 })
                .sum::<usize>()
                == 1
            {
                return lines[b]
                    .chars()
                    .zip(lines[a].chars())
                    .fold(String::new(), |acc, (a, b)| {
                        if a == b {
                            let mut res = acc;
                            res.push(a);
                            res
                        } else {
                            acc
                        }
                    });
            }
        }
    }
    unreachable!()
}
